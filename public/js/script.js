$(document).ready(function(){


 
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::::::::::::::::::::::::::::: FRONTEND ::::::::::::::::::::::::::::::*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/



    // ::::: Mobile menu ::::: //

    $('.navbar-toggle, nav').click(function(){
        $('.top-bar').toggleClass('navbar-on');
        $('nav').fadeToggle();
        $('nav').removeClass('nav-hide');
    });

	// scroll to the anchor point .worklist
	$('.button-works').click(function(){
		var worklist = $('.worklist');
		$("html, body").animate({ scrollTop: worklist.offset().top }, 2000, 'easeInOutExpo');
	});

    // fire the parallax
    $('.parallax-wrap').enllax();

	// get height 100% on the .con div
    var arrayTranslate = $('.parallax-wrap').css('transform').split(',');
    arrayTranslate = arrayTranslate[5].replace(/[)]/,'').replace(/[-]/,'');
    $('.con').css('height', 'calc(100% + ' + arrayTranslate + 'px)');
    $('.project-detail').css('height', 'calc(100vh - ' + arrayTranslate + 'px)');

    //variable that saves the scroll position
    var scrollPosition;



    // ::::: Ajax call for loading the detail ::::: //

    $('.ajaxCall').click(function(e){
    	
    	e.preventDefault();
    	
    	//loading icon
    	$('body').append('<div class="loading"></div>');

    	// get the clean URL for the call
    	var url = $(this).attr("href");
    	url = url.split('/');
    	url = url[3] + '/' + url[4];

		// get the json response from the controller 
    	$.ajax({

    		url: url,
    		success: function(data) {

    			// print the data into DIVs
    			$('body').append('<div class="project-detail">' + data + '</div>').promise().done(function(){
    				$('.loading').remove();
    				//get the scroll position before open the detail
    				scrollPosition = $(window).scrollTop() + 'px';
    				$("html, body").animate({ scrollTop: '0'}, 500, 'easeInOutExpo');
    				//var windowHeight = $(window).height() + 'px';
    				$('.project-detail').animate({top: '0', opacity: '1'}, 1000, 'easeInOutExpo', function(){
    					$('.wrap-master-list').fadeOut();
    				});
    			});

    			// call the remove ajax function defined out of this scope
    			removeAjax();

    		},
    		error: function(data) {
    			console.log('Error: ', data);
    		}

    	});

    });

    // to remove the ajax detail loaded
    function removeAjax(){
		$('#remove').on('click', function(){
			$("html, body").animate({ scrollTop: scrollPosition }, 1000, 'easeInOutExpo');
			$('.wrap-master-list').fadeIn();
	    	$(this).parent().parent().animate({top: '-100%', opacity: '0'}, 1000, 'easeInOutExpo', function(){
	    		$(this).remove();
	    	});
		});
    }


    // Apply the yellow overlay before all images are loaded

    var pageWidth = $(window).width();
    var pageHeight = $(window).height();
    var overlay = $('<div>', {style: 'width:' + pageWidth + 'px; height:' + pageHeight + 'px; position:fixed; top:0; left:0; background-color: #ffd940; z-index:500;', class: 'overlay'});

    $('body').append(overlay);
    $('.overlay').append('<div class="loading"></div>');

    
    // effect applied on the text "Desgin First"
    var wrapHeading1 = [];
    var wrapHeading2 = [];
    var characters1 =  $('h1.major > span:first-child').text().split("");
    var characters2 =  $('h1.major > span:last-child').text().split("");

    $.each(characters1, function(index, value){
        wrapHeading1.push("<em>" + value + "</em>");
    });

     $.each(characters2, function(index, value){
        wrapHeading2.push("<em>" + value + "</em>");
    });

    $('h1.major > span:first-child').html(wrapHeading1.join(''));
    $('h1.major > span:last-child').html(wrapHeading2.join(''));

    //prepare element for starting animation
    TweenMax.set( $('.face img'), {css:{opacity:0, top: '10px'}});
    TweenMax.set($('h1.major > span > em'), {alpha: 0});
    TweenMax.set($('h1.major > span:first-child'), {css:{marginRight: '-20px'}});
    TweenMax.set($('h1.major > span:last-child'), {css:{marginRight: '+20px'}});
    TweenMax.set($('h2.minor'), {alpha:0});
    TweenMax.set($('.brand'), {css:{left: '-150px'}});
    TweenMax.set($('.navbar-toggle'), {css:{right: '-150px'}});

    // when all the images are loaded, I show the content
    $(function() {
        function imageLoaded() { 

           counter--; 

           if( counter === 0 ) {
                $('.loading').remove();
                $('.overlay').fadeOut('swing', function() {

                    $(function() {

                         TweenMax.to( $('.face img'), 1,{css:{opacity:1, top: '0'}});
                         TweenMax.staggerTo( $('h1.major > span > em'), 3, {alpha:1}, 0.1 );
                         TweenMax.to( $('h1.major > span:first-child'), 1,{css:{marginRight: 0}, ease:Strong.easeInOut});
                         TweenMax.to( $('h1.major > span:last-child'), 1,{delay:0.3, css:{marginRight: 0}, ease:Strong.easeInOut});
                         TweenMax.to($('h2.minor'), 2,{delay:1.5, alpha:1});
                         TweenMax.to($('.brand'), 1,{css: {left: 0}});
                         TweenMax.to($('.navbar-toggle'), 1,{css: {right: 0}});

                    });
                });
           }

        }
        var images = $('img');
        var counter = images.length;  // initialize the counter

        images.each(function() {
            
            if(this.complete) {
                imageLoaded();
            } else { 
                $(this).one('load', imageLoaded);
            }

        });
    });
	    

})