$(document).ready(function(){


	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::::::::::::::::::::::::::::::: ADMIN :::::::::::::::::::::::::::::::*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

    // Cool Select for tags 
    $('.cool-select').select2({
        placeholder: "Select a tag"
    });

    // Swipebox - gallery lightbox
    $( '.swipebox' ).swipebox();

    // ::::: Delete confirmation with Bootstrap ::::: //
       
    $('.deletion-button').click(function(e){

        // get the specific form from the button I click
        var form=$(this).closest('form'); 
        
        // prevent submit triggered with the click action
        e.preventDefault();

        if($('#title').length !== 0) {
        	var value = $(this).parent().parent().parent().find('.form-group > div > input').val();
        } else {
	        var value = $(this).parent().parent().parent().find('td:nth-child(2)').text();
	    }
        
        $('.modal-sentence > strong').text(value);
        
        // show the modal
        $('#deletion-modal').modal('show');

        // on "delete" button trigger the submit
        $('.delete').click(function(){
            form.submit();
        });

    });


    // ::::: Sort images gallery by drag and drop ::::: //
            
    // generates an incrising loop in the order fields
    function sortArray()
    {
        $('.sortable > li').each(function(index) {
            $(this).find('.orderInput').val(index + 1);
        });
    }

    //apply the loop during the first page load
    sortArray();

    // drag and drop to sort the gallery
    $(function() {
        $( ".sortable" ).sortable({
            update: function (event, ui) {
                sortArray();
            }
        });
    });

    //input type file 
    $(':file').filestyle();

    //checkbox
    $(':checkbox').checkboxpicker({
      html: true,
      offLabel: '<span class="glyphicon glyphicon-ok">',
      onLabel: '<span class="glyphicon glyphicon-remove">',
      onActiveCls: 'btn-danger',
      offActiveCls: 'btn-success'
    });

    $('.cool-checkbox').click(function(){
        
        // opacity on the image
        if ( $(this).children('.btn-group').children('a:last-child').hasClass('btn-danger') ) {
            $(this).next().fadeTo('swing', 0.3);
        } else {
            $(this).next().fadeTo('swing', 1);
        }

        // show/hide the button
        if ( $('.cool-checkbox > div > a:last-child').hasClass('btn-danger') ) {
            $('.delete-gallery-images').fadeIn();
        } else {
            $('.delete-gallery-images').fadeOut();
        }

    });

});