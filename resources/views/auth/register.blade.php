@extends('../admin/layout/main')

@section('content')

<h1>Registration</h1>

@if (count($errors->all()) > 0)
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $message)
            <p> {{ $message }} </p>
        @endforeach
    </div>
@endif

<div class="container">
    <div class="row">
       

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="username" class="col-md-4 control-label">Name</label>
                    <div class="col-md-6">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">Email</label>
                    <div class="col-md-6">
                        <input type="text" name="email" class="form-control" id="email" placeholder="Email" value="{{ old('email') }}">
                    </div>   
                </div>
                <div class="form-group">
                    <label for="password" class="col-md-4 control-label">Password</label>
                    <div class="col-md-6">
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm</label>
                    <div class="col-md-6">
                        <input type="password" name="password_confirmation" class="form-control" id="password-confirm" placeholder="Password confirmation">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
       
    </div>
</div>


@endsection
