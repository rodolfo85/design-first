@extends('../admin/layout/main')

@section('content')

<h1>Login</h1>

@if (count($errors->all()) > 0)
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $message)
            <p> {{ $message }} </p>
        @endforeach
    </div>
@endif

<form class="form-inline" role="form" method="POST" action="{{ url('/login') }}">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" class="form-control" id="email" placeholder="Email" value="{{ old('email') }}">
    </div>
    <div class="form-group col-sm-offset-1">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
