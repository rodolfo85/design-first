
@extends('/admin/layout/main')


@section('content')


            <!-- Title -->
       		<h1>{{ $title }}</h1>


       		<!-- Success message -->
            @if (Session::has('success'))
                <div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
            @endif

       		@if (count($errors->all()) > 0)
			    <div class="alert alert-danger" role="alert">
			        @foreach ($errors->all() as $message)
			            <p> {{ $message }} </p>
			        @endforeach
			    </div>
			@endif


            <!-- form edit -->

            @if (Route::getCurrentRoute()->getPath() == 'admin/projects/edit/{id}')

                   
                        
                        <h3 class="section-title col-md-12 nopadding">Fields</h3>


                        <div class="col-md-2 pull-left sidebar nopadding">

                            <!-- delete button -->
                            @if (Route::getCurrentRoute()->getPath() == 'admin/projects/edit/{id}')
                                {{ Form::open(['url' => '/admin/projects/delete/' . $edit->id, 'role' => 'form', 'class' => 'form-deletion'] ) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    <a class="btn btn-primary col-md-12" target="_blank" href="{{ url(str_slug($edit->category->name, '-') . '/' . $edit->slug) }}"> See the project </a>
                                    {{ Form::submit('Delete project', ['class' => 'btn btn-danger deletion-button col-md-12'] ) }}
                                {{ Form::close() }}
                            @endif
                            <!-- end delete button -->

                        </div>


                        {{ Form::open(['url' => '/admin/projects/update/' . $edit->id, 'class' => 'nopadding form-horizontal col-md-9 pull-right col-md-offset-1', 'role' => 'form', 'method' => 'post', 'files' => true]) }}


                            <div>

                                <div class="form-group">
                                    {{ Form::label('title', 'Title*', ['class' => 'col-md-2 control-label']) }}
                                    <div class="col-md-9">
                                        {{ Form::text('title', $edit->title, ['class' => 'form-control', 'id' =>'title', 'required' => true]) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                        {{ Form::label('slug', 'Slug', ['class' => 'col-md-2 control-label']) }}
                                    <div class="col-md-9">
                                        {{ Form::text('slug', $edit->slug, ['class' => 'form-control', 'id' =>'slug']) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('date', 'Date*', ['class' => 'col-md-2 control-label']) }}
                                    <div class="col-md-9">
                                        {{ Form::date('date', $edit->date, ['class' => 'form-control', 'id' =>'date', 'required' => true]) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('client', 'client', ['class' => 'col-md-2 control-label']) }}
                                    <div class="col-md-9">
                                        {{ Form::text('client', $edit->client, ['class' => 'form-control', 'id' =>'client']) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('category', 'Category*', ['class' => 'col-md-2 control-label']) }}
                                    <div class="col-md-9">
                                        {{ Form::select('category', $allCategories, $edit->category->id, ['class' => 'form-control', 'id' =>'category', 'required' => true]) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('tags', 'Tags', ['class' => 'col-md-2 control-label']) }}
                                    <div class="col-md-9">
                                        {{ Form::select('tag[]', $allTags, $selected, ['class' => 'form-control cool-select', 'multiple' => true, 'id' =>'tags']) }}                  
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('image-list', 'Image list*', ['class' => 'col-md-2 control-label']) }}
                                    <div>
                                        <div class="col-md-7 col-xs-9 pull-left">
                                            {{ Form::file('image_list', ['id' => 'image-list']) }}
                                        </div>
                                        <div class="col-md-2 col-xs-3 pull-left">
                                            <div class="wrap-thumb">
                                                @if (!empty($edit->image_list))
                                                    <div class="thumb list "><img src="{{ asset('uploads/list/thumbs/' . $edit->image_list) }}" alt="{{ $edit->title }}" /></div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('description', 'Description*', ['class' => 'col-md-2 control-label']) }}
                                    <div class="col-md-9">
                                        {{ Form::textarea( 'description', $edit->description, ['class' => 'form-control ckeditor', 'rows' => '5', 'id' =>'description', 'required' => true] ) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('gallery', 'Gallery', ['class' => 'col-md-2 control-label']) }}
                                    <div class="col-md-7">
                                        {{ Form::file('gallery[]', ['id' => 'gallery', 'multiple' => true]) }}
                                    </div>
                                </div>
                                <div class="form-group col-md-5 nopadding">
                                    <div class="btn-group col-md-offset-5 col-md-9" role="group">
                                        {{ Form::button('Save', ['class' => 'btn btn-success col-md-12 update-data', 'type' => 'submit']) }}
                                    </div>
                                </div>

                            </div>


                            @if ($galleryExists)

                                <!-- gallery -->
                                <div class="col-md-12 gallery">
                                    
                                    <h3 class="section-title pull-left col-md-4">Gallery</h3>

                                    <div class="form-group delete-gallery-images pull-right col-md-2">
                                        <div role="group">
                                            {{ Form::button('Delete images', ['class' => 'btn btn-danger col-md-12', 'type' => 'submit']) }}
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <ul class="sortable wrap-thumbs">
                                        @foreach ($gallery as $key => $picture)
                                            <li class="wrap-thumb" id="{{ 'item_' .  $key}}">
                                                <div class="cool-checkbox">
                                                    {{ Form::checkbox('images_id[]', $picture->id) }}
                                                </div>
                                                <div class="thumb gallery">
                                                    <img src="{{ asset('uploads/gallery/thumbs/' . $picture->image_name) }}" alt="{{ $edit->title }}" />
                                                </div>

                                                {{ Form::hidden('images_order_id[' . $key . ']', $picture->id, ['class' => 'idInput']) }}
                                                {{ Form::hidden('weights[' . $key . ']', $picture->weight, ['class' => 'orderInput']) }}

                                            </li>
                                        @endforeach

                                    </ul>  

                                    <div class="clearfix"></div>

                                </div>
                                <!-- end gallery -->
                            @endif

                        {{ Form::close() }}

                    <div class="clearfix"></div>


            @else

                <div class="col-md-12 col-md-offset-1 nopadding">

                    <!-- form insert -->

                    {{ Form::open(['url' => '/admin/projects/store/', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST', 'files' => true]) }}
                        
                        <div>

                            <div class="form-group">
                                {{ Form::label('title', 'Title*', ['class' => 'col-md-2 control-label']) }}
                                <div class="col-md-7">
                                    {{ Form::text('title', null, ['class' => 'form-control', 'id' =>'title', 'required' => true]) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('slug', 'Slug', ['class' => 'col-md-2 control-label']) }}
                                <div class="col-md-7">
                                    {{ Form::text('slug', null, ['class' => 'form-control', 'id' =>'slug']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('date', 'Date*', ['class' => 'col-md-2 control-label']) }}
                                <div class="col-md-2">
                                    {{ Form::date('date', null, ['class' => 'form-control', 'id' =>'date', 'required' => true]) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('client', 'client', ['class' => 'col-md-2 control-label']) }}
                                <div class="col-md-7">
                                    {{ Form::text('client', null, ['class' => 'form-control', 'id' =>'client']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('category', 'Category*', ['class' => 'col-md-2 control-label']) }}
                                <div class="col-md-7">
                                    {{ Form::select('category', $allCategories, null, ['placeholder' => 'Select an option', 'class' => 'form-control', 'id' =>'category', 'required' => true]) }}                  
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('tags', 'Tags', ['class' => 'col-md-2 control-label']) }}
                                <div class="col-md-7">
                                    {{ Form::select('tag[]', $allTags, null, ['class' => 'form-control cool-select', 'multiple' => true, 'id' =>'tags']) }}                  
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('image-list', 'Image list*', ['class' => 'col-md-2 control-label']) }}
                                <div class="col-md-5">
                                    {{ Form::file('image_list', ['id' => 'image-list', 'required' => true]) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('description*', 'Description*', ['class' => 'col-md-2 control-label']) }}
                                <div class="col-md-7">
                                    {{ Form::textarea( 'description', null, ['class' => 'form-control ckeditor', 'rows' => '5', 'id' =>'description', 'required' => true] ) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('gallery', 'Gallery', ['class' => 'col-md-2 control-label']) }}
                                <div class="col-md-5">
                                    {{ Form::file('gallery[]', ['id' => 'gallery', 'multiple' => true]) }}
                                </div>
                            </div>
                            <div class="form-group col-md-5 nopadding">
                                <div class="btn-group col-md-offset-5 col-md-7" role="group">
                                    {{ Form::button('Add project', ['class' => 'btn btn-primary col-md-12', 'type' => 'submit']) }}
                                </div>
                            </div>

                        </div>
                        
                    {{ Form::close() }}

                </div>

                <div class="clearfix"></div>

            @endif


    <!-- Small modal for deleting confirmation -->
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="deletion-modal" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Delete confirmation</h4>
            </div>
            <div class="modal-body">
                
                <p class="modal-sentence">Are you sure to delete the project <strong></strong>?</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger delete">Yes</button>
            </div>

        </div>
      </div>
    </div>


@stop
