
@extends('/admin/layout/main')


@section('content')
    
   
        
    <a href="/admin/projects/add" class="btn btn-primary pull-right">
        <span>
            <i class="fa fa-pencil"></i> Add project
        </span>
    </a>

    <h1>{{ $title }}</h1>

    <!-- Success message -->
    @if (Session::has('success'))
        <div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
    @endif

    @if (count($errors->all()) > 0)
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $message)
                <p> {{ $message }} </p>
            @endforeach
        </div>
    @endif


    <h3>List</h3>

    <div class="wrap-table">

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Project</th>
                    <th>Category</th>
                    <th>Project date</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            @if (count($projects) > 0)

                @foreach ($projects as $key => $project)
                    <tr>    
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $project->title }} </td>
                        <td> {{ $project->category->name }} </td>
                        <td> {{ date('F d, Y', strtotime($project->date)) }}</td>
                        <td>  
                                
                                <!-- edit button -->
                                {{ Form::open(['url' => '/admin/projects/edit/' . $project->id, 'role' => 'form', 'class' => 'form-edit'] ) }}
                                    {{ Form::hidden('_method', 'GET') }}
                                    {{ Form::submit('Edit', ['class' => 'btn btn-default edit-button'] ) }}
                                {{ Form::close() }}
                            
                        </td>
                        <td>
                                <!-- delete button -->
                                {{ Form::open(['url' => '/admin/projects/delete/' . $project->id, 'role' => 'form', 'class' => 'form-deletion'] ) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger deletion-button'] ) }}
                                {{ Form::close() }}

                        </td>
                    </tr>
                @endforeach
                
            @else
                <tr>
                    <td colspan="7">There are no projects</td>
                </tr>
            @endif
            </tbody>
        </table>


    </div>

    <!-- Small modal -->
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="deletion-modal" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Delete confirmation</h4>
            </div>
            <div class="modal-body">
                
                <p class="modal-sentence">Are you sure to delete the project <strong></strong>?</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger delete">Yes</button>
            </div>

        </div>
      </div>
    </div>

@stop

