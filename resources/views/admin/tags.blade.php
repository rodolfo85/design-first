
@extends('/admin/layout/main')


@section('content')

<div class="container-fluid">
    <div class="row">
       
       		<h1>{{ $title }}</h1>

            <!-- Success message -->
       		@if (Session::has('success'))
			    <div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif

            <!-- Error message -->
       		@if (count($errors->all()) > 0)
			    <div class="alert alert-danger" role="alert">
			        @foreach ($errors->all() as $message)
			            <p> {{ $message }} </p>
			        @endforeach
			    </div>
			@endif
            
            @if (Route::getCurrentRoute()->getPath() == 'admin/tags/edit/{id}')
                
                <!-- Tag edit -->
                <div class="col-md-12 row">
                    {{ Form::open(['url' => '/admin/tags/update/' . $edit->id, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put'] ) }}
                        <div class="form-group col-md-5 col-sm-8 col-xs-8">
                            {{ Form::text('tag', $edit->name, ['class'=>'form-control', 'id' =>'tag', 'placeholder'=>'Edit a tag', 'required' => true] )}}
                        </div>
                        <div class="form-group">
                            {{ Form::button('Edit', ['class' => 'btn btn-primary save-button col-md-2 col-sm-4 col-xs-4', 'type' => 'submit'] ) }}
                        </div>
                    {{ Form::close() }}
                </div>

            @else

                <!-- Tag create -->
                <div class="col-md-12 row">
                    {{ Form::open(['url' => '/admin/tags/store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post'] ) }}
                        <div class="form-group col-md-5 col-sm-8 col-xs-8">
                            {{ Form::text('tag', old('tag') , ['class'=>'form-control', 'id' =>'tag', 'placeholder'=>'Create a tag', 'required' => true] )}}
                        </div>
                        <div class="form-group">
                            {{ Form::button('Create', ['class' => 'btn btn-primary save-button col-md-2 col-sm-4 col-xs-4', 'type' => 'submit'] ) }}
                        </div>
                    {{ Form::close() }}
                </div>

            @endif

            <!-- Tag list -->
            <div class="col-md-12 row">
                
                <h3>List</h3>

                    <div class="wrap-table">

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tag</th>
                                <th>Created</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if (count($tags) > 0)

                            @foreach ($tags as $key => $tag)
                                <tr>
                                    <td> {{ $key + 1 }} </td>
                                    <td> {{$tag->name}} </td>
                                    <td> {{ date('F d, Y', strtotime($tag->created_at)) }} </td>
                                    <td>
                                        
                                        {{ Form::open(['url' => '/admin/tags/edit/' . $tag->id, 'role' => 'form', 'class' => 'form-edit'] ) }}
                                            {{ Form::hidden('_method', 'GET') }}
                                            {{ Form::submit('Edit', ['class' => 'btn btn-default edit-button'] ) }}
                                        {{ Form::close() }}

                                    </td>
                                    <td>

                                        {{ Form::open(['url' => '/admin/tags/delete/' . $tag->id, 'role' => 'form', 'class' => 'form-deletion'] ) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                            {{ Form::submit('Delete', ['class' => 'btn btn-danger deletion-button'] ) }}
                                        {{ Form::close() }}
                                        
                                    </td>
                                </tr>
                            @endforeach
                            
                        @else
                            <tr>
                                <td colspan="5">There are no tags</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                </div>

            </div>



            <!-- Small modal -->
            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="deletion-modal" aria-labelledby="mySmallModalLabel">
              <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Delete confirmation</h4>
                    </div>
                    <div class="modal-body">
                        
                        <p class="modal-sentence">Are you sure to delete the tag <strong></strong>?</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-danger delete">Yes</button>
                    </div>

                </div>
              </div>
            </div>
          

    </div>
</div>

@stop