@extends('/admin/layout/main')

@section('content')

<div class="container-fluid">
    <div class="row">
       
       		<h1>{{ $title }}</h1>

       		<!-- success message -->
       		@if (Session::has('success'))
       			<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
       		@endif

       		<!-- errors -->
       		@if (count($errors->all()) > 0)
       			<div class="alert alert-danger" role="alert">
       				@foreach($errors->all() as $message)
       					<p>{{ $message }}</p>
       				@endforeach
       			</div>
       		@endif

       		<div class="filter">

	       		{{ Form::open(['url' => '/admin/media/filter/', 'role' => 'form', 'method' => 'GET']) }}
	       			<div class="form-group">
	                	{{ Form::label('projects', 'Filter by projects', ['class' => 'col-md-12 control-label']) }}
	                  	<div class="col-md-5">
		                  	@unless (Request::has('project'))	
		                  		{{ Form::select('project', $allProjects, null, ['class' => 'form-control', 'id' =>'category']) }}
		                  	@else
		                  		{{ Form::select('project', $allProjects, $project->id, ['class' => 'form-control', 'id' =>'category']) }}
		                  	@endunless
		                </div>
	                </div>
	                <div class="form-group btn-group col-md-3" role="group">
                        {{ Form::button('Filter', ['class' => 'btn btn-primary', 'type' => 'submit']) }}
                    </div>
                    <div class="clearfix"></div>
	       		{{ Form::close() }}

       		</div>

       		

	       		<!-- if filtering -->
	       		<ul class="wrap-projects">

			       	<!-- if normal view -->
		       		@unless (Request::has('project'))
		       			@foreach ($projects as $project)
		       				<h2>{{ $project->title }}</h2>
		       				<li>
					       		<ul class="wrap-thumbs">
					       			@foreach ($project->gallery as $image)
					       				<li class="wrap-thumb">
					       					<div class="thumb gallery">
					       						<a href="{{ asset('/uploads/gallery/' . $image->image_name) }}" class="swipebox">
					       							<img src="{{ asset('/uploads/gallery/thumbs/' . $image->image_name) }}" alt="{{ $image->image_name }}" />
					       						</a>
					       					</div>
					       				</li>
					       			@endforeach
					       		</ul>
				       		</li>
				       	@endforeach

       				@else

 						<h2>{{ $project->title }}</h2>
		   				<li>
				       		<ul class="wrap-thumbs">
				       			@foreach ($gallery as $image)
				       				<li class="wrap-thumb">
				       					<div class="thumb gallery">
				       						<img src="{{ asset('/uploads/gallery/thumbs/' . $image->image_name) }}" alt="{{ $image->image_name }}" />
				       					</div>
				       				</li>
				       			@endforeach
				       		</ul>
			       		</li>

		       		@endunless

	       		</ul>

    </div>
</div>



@stop