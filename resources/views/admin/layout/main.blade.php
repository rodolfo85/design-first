<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Rodolfo Ferro Casagrande, Web Design Portfolio. Web site design and development in Calgary, Canada">
        <meta name="keywords" content="Rodolfo Ferro Casagrande, Calgary, Canada, Drupal development, Web Design, Frontend development">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/images/favicon.png">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,400i,900" rel="stylesheet">
        <link rel="stylesheet" href="/css/icons/font-awesome.css">
        <link rel="stylesheet" href="/css/jquery-ui.min.css">
        <link rel="stylesheet" href="/css/swipebox.min.css">
        <link rel="stylesheet" href="/css/app.css">
        <script src="/js/jquery-2.1.3.js"></script>
        <script type="text/javascript" src="/js/jquery-ui.min.js"> </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
        <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
        <script type="text/javascript" src="/js/bootstrap-filestyle.min.js"> </script>
        <script type="text/javascript" src="/js/jquery.swipebox.min.js"> </script>  
        <script type="text/javascript" src="/js/bootstrap-checkbox.js"> </script>
        <script type="text/javascript" src="/js/script-admin.js"> </script>

        <title>{{ (Auth::guest()) ? 'Admin' : $title }}</title>
    </head>
    <body>
        <div class="container">
            <header>
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header logo col-md-12">
                            <a class="navbar-brand icon-logo-design-first" href="/admin/projects">Design First</a>
                            <div class="pull-right">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            @if (Auth::guest())
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="{{ url('/login') }}">Login</a></li>
                                    <li><a href="{{ url('/register') }}">Register</a></li>
                                </ul>
                            @else
                                <ul class="nav navbar-nav">
                                    <li {{ activeMenu('#admin\/projects') }} ><a href="/admin/projects">Projects</a></li>
                                    <li {{ activeMenu('#admin\/categories') }} ><a href="/admin/categories">Categories</a></li>
                                    <li {{ activeMenu('#admin\/tags') }} ><a href="/admin/tags">Tags</a></li>
                                    <li {{ activeMenu('#admin\/media') }} ><a href="/admin/media">Media</a></li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="welcome"><a href="#">Hello, {{ Auth::user()->name }}</a></li>
                                    <li><a href="{{ url('/logout') }}">Logout</a></li>
                                </ul>
                            @endif

                        </div>
                    </div>
                </nav>
            </header>


            <div class="content">

                @yield('content')

            </div>
            
            <footer>
                <div class="navbar navbar-default">
                    <p>© Copyright Design first {{ Date('Y') }}</p>
                </div>
            </footer>

        </div>   
    </body>
</html>