@extends('/projects/layout/main')

@section('content')
	

	<div class="wrap-master-list">

		<div class="parallax-wrap" data-enllax-ratio="-0.4" data-enllax-type="foreground">
			<div class="taglines">
				<h2 class="minor">
					<span>Design</span>
					<span>the shape</span>
					<span>of ideas</span>
				</h2>
				<h1 class="major">
					<span>Design</span>
					<span>First</span>
				</h1>
			</div>
			<div class="parallax-face face" data-enllax-ratio="0.3" data-enllax-type="foreground">
				<picture>
					<source media="(max-width: 45em)" srcset="/images/face-small.png">
					<source media="(max-width: 90em)" srcset="/images/face-medium.png">
					<img src="/images/face-original.png" alt="Splash photo of the face of Rodolfo Ferro Casagrande">
				</picture>
			</div>
			<div class="button-works">
				<span class="text">Works</span>
				<span class="pe-7s-angle-down st"></span>
			</div>

			<!-- 
			<div class="con">
			  <div class="stripes">
			    <hr class="sto st1 asf"/>
			    <hr class="sto st2"/>
			    <hr class="sto st3 asf"/>
			    <hr class="sto st4"/>
			    <hr class="sto st5"/>
			    <hr class="sto st6"/>
			    <hr class="sto st5 asf"/>
			    <hr class="sto st4"/>
			    <hr class="sto st3"/>
			    <hr class="sto st2"/>
			    <hr class="sto st1 asf"/>
			    <hr class="sto st2"/>
			    <hr class="sto st3"/>
			    <hr class="sto st4 asf"/>
			    <hr class="sto st5"/>
			    <hr class="sto st6 asf"/>
			    <hr class="sto st5 asf"/>
			    <hr class="sto st4"/>
			    <hr class="sto st3"/>
			    <hr class="sto st2 asf"/>
			    <hr class="sto st1"/>
			    <hr class="sto st2"/>
			    <hr class="sto st3 asf"/>
			    <hr class="sto st4"/>
			    <hr class="sto st5 asf"/>
			    <hr class="sto st6"/>
			    <hr class="sto st5"/>
			    <hr class="sto st4 asf"/>
			    <hr class="sto st3"/>
			    <hr class="sto st2 asf"/>
			    <hr class="sto st1 asf"/>
			    <hr class="sto st2"/>
			    <hr class="sto st3 asf"/>
			    <hr class="sto st4"/>
			    <hr class="sto st5"/>
			    <hr class="sto st6 asf"/>
			    <hr class="sto st5"/>
			    <hr class="sto st4 asf"/>
			    <hr class="sto st3"/>
			    <hr class="sto st2 asf"/>
			    <hr class="sto st1"/>
			    <hr class="sto st2"/>
			    <hr class="sto st3"/>
			    <hr class="sto st4 asf"/>
			    <hr class="sto st5"/>
			    <hr class="sto st6 asf"/>
			    <hr class="sto st5"/>
			    <hr class="sto st4 asf"/>
			    <hr class="sto st3"/>
			    <hr class="sto st2 asf"/>
			 
			  </div>
			  <div class="stripes2">
			    <hr class="sto st1 asf"/>
			    <hr class="sto st2"/>
			    <hr class="sto st3 asf"/>
			    <hr class="sto st4"/>
			    <hr class="sto st5"/>
			    <hr class="sto st6"/>
			    <hr class="sto st5 asf"/>
			    <hr class="sto st4"/>
			    <hr class="sto st3"/>
			    <hr class="sto st2"/>
			    <hr class="sto st1 asf"/>
			    <hr class="sto st2"/>
			    <hr class="sto st3"/>
			    <hr class="sto st4 asf"/>
			    <hr class="sto st5"/>
			    <hr class="sto st6 asf"/>
			    <hr class="sto st5 asf"/>
			    <hr class="sto st4"/>
			    <hr class="sto st3"/>
			    <hr class="sto st2 asf"/>
			    <hr class="sto st1"/>
			    <hr class="sto st2"/>
			    <hr class="sto st3 asf"/>
			    <hr class="sto st4"/>
			    <hr class="sto st5 asf"/>
			    <hr class="sto st6"/>
			    <hr class="sto st5"/>
			    <hr class="sto st4 asf"/>
			    <hr class="sto st3"/>
			    <hr class="sto st2 asf"/>
			    <hr class="sto st1 asf"/>
			    <hr class="sto st2"/>
			    <hr class="sto st3 asf"/>
			    <hr class="sto st4"/>
			    <hr class="sto st5"/>
			    <hr class="sto st6 asf"/>
			    <hr class="sto st5"/>
			    <hr class="sto st4 asf"/>
			    <hr class="sto st3"/>
			    <hr class="sto st2 asf"/>
			    <hr class="sto st1"/>
			    <hr class="sto st2"/>
			    <hr class="sto st3"/>
			    <hr class="sto st4 asf"/>
			    <hr class="sto st5"/>
			    <hr class="sto st6 asf"/>
			    <hr class="sto st5"/>
			    <hr class="sto st4 asf"/>
			    <hr class="sto st3"/>
			    <hr class="sto st2 asf"/>
			  </div>
			</div>
			-->
		</div>

		<div class="wrap-ajax">

			<!-- works list -->
			<ul class="worklist" data-enllax-ratio="0" data-enllax-type="foreground">
				<h1>worklist here</h1>

				@foreach ($projects as $project)
					<dl>
						<dt><a class="ajaxCall" href="{{ url(str_slug($project->category->name, '-') . '/' . $project->slug) }}">{{ $project->title }} </a></dt>
						<dd>{!! $project->description !!}</dd>
					</dl>
				@endforeach
			</ul>

		</div>

	</div>	

	<script>
		$(document).ready(function(){

	

		});

	</script>

@stop