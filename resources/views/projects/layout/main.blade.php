<!doctype html>
<html>
    <head>

        <!-- Metatags -->
        <meta charset="utf-8">
        <meta name="description" content="Rodolfo Ferro Casagrande, Web Design Portfolio. Web site design and development in Calgary, Canada">
        <meta name="keywords" content="Rodolfo Ferro Casagrande, Calgary, Canada, Drupal development, Web Design, Frontend development">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="index, follow">
        <meta name="author" content="Rodolfo Ferro Casagrande">

        <!-- Favicons -->
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/images/favicons/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/favicons/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/favicons/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/favicons/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/images/favicons/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/images/favicons/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/images/favicons/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/images/favicons/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="/images/favicons/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="/images/favicons/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="/images/favicons/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="/images/favicons/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="/images/favicons/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="&nbsp;"/>
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="/images/favicons/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="/images/favicons/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="/images/favicons/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="/images/favicons/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="/images/favicons/mstile-310x310.png" />

        <!-- Open Graph -->
        <meta property="og:type" content="profile">
        <meta property="fb:app_id" content="390722761280733" />
        <meta property="og:title" content="Web designer, Frontend developer">
        <meta property="og:url" content="http://www.design-first.it">
        <meta property="og:image" content="http://www.design-first.it/public/images/fb-cover.jpg">
        <meta property="profile:first_name" content="Rodolfo">
        <meta property="profile:last_name" content="Ferro Casagrande">
        <meta property="og:description" content="Rodolfo Ferro Casagrande, Web Design Portfolio. Web site design and development in Calgary, Canada" />

        <!-- Assets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,400i,900" rel="stylesheet">
        <link rel="stylesheet" href="/css/mobile-menu.css">
        <link rel="stylesheet" href="/css/stroke-icon.css">
        <link rel="stylesheet" href="/css/frontend.css">
        <script src="/js/jquery-2.1.3.js"></script>
        <script src="/js/jquery-ui.min.js"></script>
        <script src="/js/TweenMax.min.js"></script>
        <script src="/js/jquery.easing.min.js"></script>
        <script src="/js/jquery.enllax.min.js"></script>
        <script src="/js/jquery.smooth-scroll.js"></script>
        <script src="/js/script.js"></script>
        
        <title>{{ $title }}</title>
    </head>
    <body class="{{ str_slug($title, '-') }}">
        <a href="#main-content" class="visually-hidden">Skip to main content</a>
        
        <!-- container -->
        <div class="container">
            
            <!-- header -->
            <header class="top-bar">
                <div class="sub-wrap">
                    <div class="brand">
                        <a href="/" class="icon-logo-design-first" title="Logo of the website, click to go to the homepage">Design First</a>
                    </div>
                    <div class='navbar-toggle' title='Menu'>
                        <div class='bar1'></div>
                        <div class='bar2'></div>
                        <div class='bar3'></div>
                    </div>
                </div>
                <nav class="main-nav nav-hide">
                    <ul>
                        <li> <a href="/" title="Homepage">Home</a> </li>
                        <li> <a href="/design-works" title="List of design works">Design works</a> </li>
                        <li> <a href="/about" title="Brief bio about Rodolfo Ferro Casagrande">About</a> </li>
                        <li> <a href="/contacts" title="Contact page">Contacts</a> </li>
                    </ul>
                </nav>        
            </header>

            <!-- content -->
            <div class="content offset" role="main" id="main-content">

                @yield('content')

            </div>
            
            <!-- footer -->
            <footer>
                <p>© Copyright Design first {{ Date('Y') }}</p>
            </footer>

        </div>
    </body>
</html>