	<wrap class="detail">

		<div id="remove" style="font-size:60px; font-weight:300; float:right; cursor:pointer">X</div>		

		<h2>This is the detail of a project</h2>
		<div class="detail">

		<h1> {{ $detail->title }} </h1>
		
		<div>{{ $detail->client }}</div>
		<div>{{ $detail->date }}
		
		<ul>    
			@foreach ($detail->tag as $singleTag)
				<li>{{ $singleTag->name }}</li>
			@endforeach
		</ul>

		<div>{!! $detail->description !!}</div>

		<ul>    
			@foreach ($imagesGallery as $image)
				<li style="width:200px; height:auto; display:inline-block; margin:0 15px;"><img src="{{ asset('uploads/gallery/thumbs/' . $image->image_name) }}" alt="{{ $image->image_name }}" ></li>
			@endforeach
		</ul>

	</div>
