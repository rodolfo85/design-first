<!DOCTYPE html>
<html>
    <head>
        <title>Error 404 - Page not found</title>
        <!-- Metatags -->
        <meta charset="utf-8">
        <meta name="description" content="Rodolfo Ferro Casagrande, Web Design Portfolio. Web site design and development in Calgary, Canada">
        <meta name="keywords" content="Rodolfo Ferro Casagrande, Calgary, Canada, Drupal development, Web Design, Frontend development">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="nofollow">
        <meta name="author" content="Rodolfo Ferro Casagrande">

        <!-- Resources -->
        <link rel="stylesheet" href="/css/stroke-icon.css">
        <link rel="stylesheet" href="/css/page404.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,400i,900" rel="stylesheet">
        <link rel="icon" href="/images/favicon.png">
    </head>
    <body class="page404">
        <div class="container">
            <div class="content">
                <div class="wrapper">
                    <div class="col">
                        <span class="t404">404 - Page not found</span>
                        <h1>
                            <span>You</span>
                            <span>got</span>
                            <span>lost</span>
                        </h1>
                        <a href="/" title="You got lost. Hit the link Take me home">
                            <span>Take me home</span>
                            <span class="pe-7s-angle-right st"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
 