var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	mix.sass('./resources/assets/sass/app.scss', './public/css/app.css')
       .sass('./resources/assets/sass/stroke-icon.scss', './public/css/stroke-icon.css')
       .sass('./resources/assets/sass/frontend.scss', './public/css/frontend.css')
       .sass('./resources/assets/sass/page404.scss', './public/css/page404.css');
});
