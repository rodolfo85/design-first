<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Tag;
use Session;

class adminTagController extends Controller
{
    public function index()
    {

    	$tags = Tag::orderBy('name')->get();
    	return view('admin.tags', ['tags' => $tags, 'title' => 'Tags']);
    }

    public function store(Request $data)
    {

    	$this->validate($data, [

    		'tag' => 'required|unique:tags,name|min:3'

    	]);

    	$post = new Tag;
    	$post->name = $data->tag;
    	$post->save();
		
    	Session::flash('success', 'You have succesfully created a new Tag');
    	return redirect('/admin/tags');

    }
    
    public function delete($id)
    {
    	$post = Tag::find($id);
        $post->project()->detach();
    	$post->delete();

    	Session::flash('success', 'The tag has been deleted');

    	return redirect('/admin/tags');
    }

    public function edit(Request $data)
    {
        $edit = Tag::find($data->id);
        $tags = Tag::orderBy('name')->get();
        return view('admin.tags', ['edit' => $edit, 'tags' => $tags, 'title' => 'Edit tags']);
    }

    public function update(Request $data)
    {
        $this->validate($data, [

            'tag' => 'required|max:255|min:3'

        ]);

        $update = Tag::find($data->id);
        $update->name = $data->tag;
        $update->save();

        Session::flash('success', 'The tag has been updateed');

        return redirect('/admin/tags');
    }

    // make it private
    public function __construct()
    {
        $this->middleware('auth');
    }

}
