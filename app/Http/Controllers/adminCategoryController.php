<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;
use App\Project;
use Session;

class adminCategoryController extends Controller
{
    

	public function index()
    {
    	$categories = Category::orderBy('name')->get();;
    	return view('admin.categories', ['categories' => $categories, 'title' => 'Categories']);
    }

    public function store(Request $data)
    {

    	$this->validate($data, [

    		'category' => 'required|unique:categories,name|min:3'

    	]);

    	$post = new Category;
    	$post->name = $data->category;
    	$post->save();
		
    	Session::flash('success', 'You have succesfully created a new Category');
    	return redirect('/admin/categories');

    }
    
    public function delete($id)
    {
    	$post = Category::find($id);

        $referenceCount = Project::where('category_id', $post->id)->count();
        $referenceProjects = Project::where('category_id', $post->id)->get();

        if($referenceCount > 0) {
            
            foreach ($referenceProjects as $key => $project) {
                $projectsTitle[] = $project->title;
            }
            
            Session::flash('warning', 'The category is already referenced to: ' . join($projectsTitle, ', ') . '. Unlink the category before deliting it.');
            
            return redirect('/admin/categories');
        
        } else {
            
            $post->delete();

        	Session::flash('success', 'The category has been deleted');

        	return redirect('/admin/categories');
        }

    }

    public function edit(Request $data)
    {
        $edit = Category::find($data->id);
        $categories = Category::orderBy('name')->get();
        return view('admin.categories', ['edit' => $edit, 'categories' => $categories, 'title' => 'Edit categories']);
    }

    public function update(Request $data)
    {
        $this->validate($data, [

            'category' => 'required|max:255|min:3'

        ]);

        $update = Category::find($data->id);
        $update->name = $data->category;
        $update->save();

        Session::flash('success', 'The category has been updateed');

        return redirect('/admin/categories');
    }

    // make it private
    public function __construct()
    {
        $this->middleware('auth');
    }
}


