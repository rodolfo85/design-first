<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Gallery;
use App\Project;


class adminMediaController extends Controller
{
    public function getProjects()
    {
    	$arrayCategories = Project::lists('title', 'id')->prepend('All', 'all');
    	return $arrayCategories;
    }

	public function index()
	{
		$projects = Project::orderBy('title')->get();
		$allProjects = $this->getProjects();
    	return view('/admin/media', ['title' => 'Media Manager', 'projects' => $projects, 'allProjects' => $allProjects]);
	}

	public function filter(Request $data)
	{

		if($data->input('project') == 'all') {
		
			return redirect('admin/media');
		
		} else {

			$project = Project::find($data->project);
			$allProjects = $this->getProjects();
			$gallery = $project->gallery()->orderBy('weight', 'ASC')->get();

			return view('admin/media', ['title' => 'Media Manager', 'project' => $project, 'allProjects' => $allProjects, 'gallery' => $gallery]);
	
		}
	}

	// make it private
    public function __construct()
    {
        $this->middleware('auth');
    }

}
