<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\JsonResponse;
use App\Project;

class projectController extends Controller
{
    public function all()
    {
    	
    	$projects = Project::all();
    	return view('projects.list', ['projects' => $projects, 'title' => 'Home']);

    }

    public function detail($category, $slug)
    {
        $detail = Project::where('slug', $slug)->first();
        $imagesGallery = $detail->gallery()->orderBy('weight', 'ASC')->get();
        return view('projects.detail', ['title' => 'Detail', 'detail'=> $detail, 'imagesGallery' => $imagesGallery]);
    }
}
