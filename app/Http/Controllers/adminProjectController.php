<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Project;
use App\Category;
use App\Tag;
use App\Gallery;
use Session;
use Image;
use Storage;
use File;

class adminProjectController extends Controller
{
	
    // project list
    public function index()
    {	
    	$projects = Project::orderBy('date', 'DESC')->get();
    	return view('admin.index', ['projects' => $projects, 'title' => 'Projects']);
    }

    // project detail create form
    public function create()
    {
        $projects = Project::all();
        $allCategories = Category::lists('name', 'id');
        $allTags = Tag::lists('name', 'id');
    	
        return view('admin.add_project', [
            
            'projects' => $projects,
            'title' => 'Add project',
            'allCategories' => $allCategories,
            'allTags' => $allTags

        ]);
    }

    // project detail edit form
    public function edit(Request $data)
    {
        $edit = Project::find($data->id);
        
        $projects = Project::all();
        $allCategories = Category::lists('name', 'id');
        $allTags = Tag::lists('name', 'id');
        $gallery = $edit->gallery()->orderBy('weight', 'ASC')->get();

        // needed to check if gallery exists
        $galleryExists = Gallery::where('project_id', $data->id)->first();

        if(count($edit->tag) > 0) {
            foreach ($edit->tag as $tag) {
                $selected[] = $tag->id;
            }
        } else {
            $selected[] = null;
        }   

        return view('admin.add_project', [

            'edit' => $edit,
            'projects' => $projects,
            'title' => 'Edit project',
            'allCategories' => $allCategories,
            'allTags' => $allTags,
            'selected' => $selected,
            'galleryExists' => $galleryExists,
            'gallery' => $gallery

        ]);
    }

    // project detail delete
    public function delete(Request $data)
    {
        $post = Project::find($data->id);
        $post->tag()->detach();

        // delete the actual file (gallery) in the filesystem
        foreach ($post->gallery->pluck('image_name') as $key => $image) {
            // delete the originals
            File::delete(public_path() . '/uploads/gallery/thumbs/' . $image);
            File::delete(public_path() . '/uploads/gallery/' . $image);
        }

        // delete the actual file (image-list) in the filesystem
        $image = $post->image_list;
        File::delete(public_path() . '/uploads/list/thumbs/' . $image);
        File::delete(public_path() . '/uploads/list/' . $image);

        $post->delete();

        Session::flash('success', 'The project has been deleted');

        return redirect('admin/projects');
    }

    // project detail update form
    public function update(Request $data)
    {

        $this->validate($data, [

            'title' => 'required|min:3|max:255|unique:projects,title,' . $data->id,
            'slug' =>'min:3|max:255|unique:projects,slug,' . $data->id,
            'date' => 'required|before:now',
            'client' => 'min:3|max:255',
            'category' => 'required',
            'image_list' => 'image|max:2000',
            'description' => 'required|min:3',
            'gallery.*' => 'image|max:2000'
        ],[
            'gallery.*.image' => 'The gallery must be an image',
            'gallery.*.max' => 'The gallery may not be greater than 1000 kilobytes.'
        ]);

        $update = Project::find($data->id);
        $update->title = $data->title;
        $update->slug = $this->slug($data, $data->slug);
        $update->date = $data->date;
        $update->client = $data->client;
        $update->category_id = $data->category;
        $update->description = $data->description;
        
        // :::::::::::: IMAGE LIST MANAGEMENT :::::::::::::

        if ($data->hasFile('image_list')) {

            $image = $data->file('image_list');
            $filename = 'image-list-' . urlirize($data->title) . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/list/' . $filename);
            $locationThumb = public_path('uploads/list/thumbs/' . $filename);
            
            // create the original image
            Image::make($image)->save($location);

            // create the thumb image
            Image::make($image)->fit(300, 300)->save($locationThumb);
            
            $update->image_list = $filename;
        }

        $update->save();


        // :::::::::::: GALLERY MANAGEMENT :::::::::::::

        if ($data->hasFile('gallery')) {
            
            $updateGal = new Gallery;
            
            $images = $data->file('gallery');

            foreach($images as $key => $image) {

                $filename = $update->id . '-' . urlirize($data->title) . '-' . time('now') . '-' . ($key + 1) . '.' . $image->getClientOriginalExtension();
                $location = public_path('uploads/gallery/' . $filename);
                $locationThumb = public_path('uploads/gallery/thumbs/' . $filename);
                Image::make($image)->save($location); 
                Image::make($image)->fit(300, 300)->save($locationThumb);
            

                // storing all the filenames in different records in one shot
                $updateGal->create(['project_id' => $data->id, 'image_name' => $filename]);

            }
        }

        // :::::::::::: TAG MANAGEMENT :::::::::::::

        if (empty($data->tag)) {
            // detach all the tags
            $update->tag()->detach();
        } else {
            // associate the tags to the project
            $update->tag()->sync($data->tag, true);
        }


        // ::::::::::::    ORDER MANAGEMENT :::::::::::::

        // sortable fields to order the gallery
        // If gallery exists AND I'm not deleting a gallery image AND I'm not creating a gallery
        if ($update->gallery->where('project_id', $update->id)->first() AND empty($data->images_id) AND !$data->hasFile('gallery')) {

            foreach ($data->weights as $key => $weight) {

                $updateWeight = Gallery::find($data->images_order_id[$key]);
                $updateWeight->weight = $weight;
                $updateWeight->save();              

            }
        }

        // delete gallery images if checked
        if (!empty($data->images_id)) {

            $delSome = Project::find($data->id);          
            
            // get the selected images to delete
            foreach ($data->images_id as $key => $image_id) {
                
                // get the current project
                foreach ($delSome->gallery as $key => $image) {

                    // get the filenames and delete the actual files
                    $imagesToDelete = $image->where('id', $image_id)->pluck('image_name')->first();
                    
                    // delete the actual file (original) in the filesystem
                    File::delete(public_path() . '/uploads/gallery/' . $imagesToDelete);
                    // delete the actual file (thumb) in the filesystem
                    File::delete(public_path() . '/uploads/gallery/thumbs/' . $imagesToDelete);

                    // delete the record in the reference table
                    $image->where('id', $image_id)->delete();
                    
                }

            }

            Session::flash('success', 'The gallery has been updateed');

            return redirect('admin/projects/edit/' . $data->id);

        } else {

            Session::flash('success', 'The project has been updateed');

        }

        return redirect('admin/projects/edit/' . $data->id);

    }

    // // Project detail store
    public function store(Request $data)
    {

        $this->validate($data, [

            'title' => 'required|min:3|max:255|unique:projects,title',
            'slug' =>'min:3|max:255|unique:projects,slug',
            'date' => 'required|before:now',
            'client' => 'min:3|max:255',
            'category' => 'required',
            'image_list' => 'required|image|max:2000',
            'description' => 'required|min:3',
            'gallery.*' => 'image|max:2000'
        ],[
            'gallery.*.image' => 'The gallery must be an image',
            'gallery.*.max' => 'The gallery may not be greater than 1000 kilobytes.'
        ]);

        $post = new Project;
        $post->title = $data->title;
        $post->slug = $this->slug($data, $data->slug);
        $post->date = $data->date;
        $post->client = $data->client;
        $post->category_id = $data->category;
        $post->description = $data->description;

        

        // :::::::::::: IMAGE LIST MANAGEMENT :::::::::::::

        if ($data->hasFile('image_list')) {

            $image = $data->file('image_list');
            $filename = 'image-list-' . urlirize($data->title) . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/list/' . $filename);
            $locationThumb = public_path('uploads/list/thumbs/' . $filename);
            
            // create the original image
            Image::make($image)->save($location);

            // create the thumb image
            Image::make($image)->fit(300, 300)->save($locationThumb);
            
            $post->image_list = $filename;
        }
        
        $post->save();



        // :::::::::::: GALLERY MANAGEMENT :::::::::::::

        if ($data->hasFile('gallery')) {
            
            $postGal = new Gallery;
            
            $images = $data->file('gallery');

            foreach($images as $key => $image) {

                $filename =  urlirize($data->title) . '-' . time('now') . '-' . ($key + 1) . '.' . $image->getClientOriginalExtension();
                $location = public_path('uploads/gallery/' . $filename);
                $locationThumb = public_path('uploads/gallery/thumbs/' . $filename);
                Image::make($image)->save($location); 
                Image::make($image)->fit(300, 300)->save($locationThumb);

                $postGal->create(['project_id' => $post->id, 'image_name' => $filename]);

            }
            
        }

        
        // :::::::::::: TAG MANAGEMENT :::::::::::::


        if (empty($data->tag)) {
            // detach all the tags
            $post->tag()->detach();
        } else {
            // associate the tags to the project
            $post->tag()->sync($data->tag, false);
        }

        
		Session::flash('success', 'You have succesfully created a new project');

        return redirect('/admin/projects');		

    }

    // make it private
    public function __construct()
    {
        $this->middleware('auth');
    }


    // Slug URL method
    public function slug($data, $slug) {
        if ($slug == '') {
            return str_slug($data->title);
        } else {
            return str_slug($slug);
        }
    }

}
