<?php

function activeMenu($givenRoute)
{
	$regex = '\/(.*)#';
	$regexComplete = $givenRoute . $regex;
	$currentRoute = Route::getCurrentRoute()->getPath();

	$matchSubsection = preg_match($regexComplete, $currentRoute);
	$matchSection = preg_match($givenRoute . '#', $currentRoute);

	if ($matchSubsection || $matchSection) {
		return "class=active";
	}
}

function urlirize($value)
{
	$string = strtolower($value);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
	
	return $string;
}
