<?php


/*---- Auth ------*/
Route::auth();



/*
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:::::::::::::::::::::::::: ADMINISTRATION ::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
*/


//::::::::::::::::: PROJECTS  :::::::::::::::::

// Project list
Route::get('/admin/projects', 'adminProjectController@index');

// Project detail store
Route::post('/admin/projects/store', 'adminProjectController@store');

// Project detail delete
Route::delete('/admin/projects/delete/{id}', 'adminProjectController@delete');

// Project detail create form
Route::get('/admin/projects/add', 'adminProjectController@create');

// Project edit
Route::get('/admin/projects/edit/{id}', 'adminProjectController@edit');

// Project update
Route::post('/admin/projects/update/{id}', 'adminProjectController@update');




//::::::::::::::::: CATEGORY  :::::::::::::::::

// Category list
Route::get('/admin/categories', 'adminCategoryController@index');

// Category detail store
Route::post('/admin/categories/store', 'adminCategoryController@store');

// Tag delete
Route::delete('/admin/categories/delete/{id}', 'adminCategoryController@delete');

// Tag edit
Route::get('/admin/categories/edit/{id}', 'adminCategoryController@edit');

// Tag edit
Route::put('/admin/categories/update/{id}', 'adminCategoryController@update');




//::::::::::::::::: TAG  :::::::::::::::::

// Tag list
Route::get('/admin/tags', 'adminTagController@index');

// Tag detail store
Route::post('/admin/tags/store', 'adminTagController@store');

// Tag delete
Route::delete('/admin/tags/delete/{id}', 'adminTagController@delete');

// Tag edit
Route::get('/admin/tags/edit/{id}', 'adminTagController@edit');

// Tag edit
Route::put('/admin/tags/update/{id}', 'adminTagController@update');



//::::::::::::::::: MEDIA  :::::::::::::::::

// Media list
Route::get('/admin/media', 'adminMediaController@index');

// Media filter
Route::get('/admin/media/filter/', 'adminMediaController@filter');



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middelwere' => ['web']], function(){

	Route::get('/', 'projectController@all');
	Route::get('/{category}/{slug}', 'projectController@detail')->where('slug', '[\w\d\-\_]+');

});

