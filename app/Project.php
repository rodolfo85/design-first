<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	public function category()
	{
		return $this->belongsTo('App\Category');
	}

	public function gallery()
	{
		return $this->hasMany('App\Gallery');
	}

    public function tag()
    {
    	return $this->belongsToMany('App\Tag', 'project_tag')->withTimestamps();
    }
}
